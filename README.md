# Splotchbot Studios #

We are a game development company that started in Florida. Our members live all over the place - and we are looking for help! Email the president by using the contact form on the website and we will get back to you as soon as possible.

# How to Contribute #

If you have any suggestions, we would be more than happy to read about them. Shoot us a message through the contact form on our website.