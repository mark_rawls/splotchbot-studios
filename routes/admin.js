var express = require('express');
var sqlite3 = require('sqlite3');
var router = express.Router();
var blogDB = new sqlite3.Database('db/posts.db');
var brookeDB = new sqlite3.Database('db/brooke.db');
var bcrypt = require('bcrypt');

// Authentication middleware
router.use(function(req, res, next) {
	if (typeof(req.session.failedattempts) === 'undefined') {
		req.session.failedattempts = 0;
	}
	if (req.session.admin === true) {
		next('route');
	} else {
		res.render('admin/nocredentials', { title: "Not logged in | Splotchbot Studios" });
	}
});

// Home page
router.get('/', function(req, res, next) {
	brookeDB.all('SELECT * FROM tasks', function(err, rows) {
		console.log(rows);
		res.render('admin/tasks', { title: 'Admin Panel | Splotchbot Studios', tasks: rows.reverse()});
	});
});

// Show tasks
router.get('/tasks', function(req, res, next) {
	brookeDB.all('SELECT * FROM tasks', function(err, rows) {
		console.log(rows);
		res.render('admin/tasks', { title: 'Tasks | Splotchbot Studios', tasks: rows.reverse()});
	});
});

// Submit new task
router.post('/tasks/new', function(req, res, next) {
	var v = req.body;
	var statement = brookeDB.prepare('INSERT INTO tasks (name, priority, status, due) VALUES (?, ?, ?, ?)');
	statement.run(v.name, v.priority, v.status, v.due);
	statement.finalize();
	res.redirect('/admin/tasks');
});

// Pull up editor page
router.get('/tasks/:id', function(req, res, next) {
	brookeDB.get('SELECT * FROM tasks WHERE id = ?', req.params.id, function(err, row) {
		res.render('admin/taskedit', { title: 'Edit task | Splotchbot Studios', task: row });
	});
});

// Submit editor thingy
router.post('/tasks/:id', function(req, res, next) {
	var v = req.body;
	var statement = brookeDB.prepare('UPDATE tasks SET name = ?, priority = ?, status = ?, due = ? WHERE id = ?');
	statement.run(v.name, v.priority, v.status, v.due, req.params.id);
	statement.finalize();
	res.redirect('/admin/tasks');
});

// Delete the task
router.get('/tasks/delete/:id', function(req, res, next) {
	var statement = brookeDB.prepare('DELETE FROM tasks WHERE id = ?');
	statement.run(req.params.id);
	statement.finalize();
	res.redirect('/admin/tasks');
});

// Show notes
router.get('/notes', function(req, res, next) {
	brookeDB.all('SELECT * FROM notes', function(err, rows) {
		res.render('admin/notes', { title: 'Notes | Splotchbot Studios', notes: rows.reverse() });
	});
});

// Submit new note
router.post('/notes/new', function(req, res, next) {
	var v = req.body;
	var statement = brookeDB.prepare('INSERT INTO notes (title, content) VALUES (?, ?)');
	statement.run(v.title, v.content);
	statement.finalize();
	res.redirect('/admin/notes');
});

// Pull up editor page
router.get('/notes/:id', function(req, res, next) {
	brookeDB.get('SELECT * FROM notes WHERE id = ?', req.params.id, function(err, row) {
		res.render('admin/noteedit', { title: 'Edit note | Splotchbot Studios', note: row });
	});
});

// Submit editor thingy
router.post('/notes/:id', function(req, res, next) {
	var v = req.body;
	var statement = brookeDB.prepare('UPDATE notes SET title = ?, content = ? WHERE id = ?');
	statement.run(v.title, v.content, req.params.id);
	statement.finalize();
	res.redirect('/admin/notes');
});

// Delete the note
router.get('/notes/delete/:id', function(req, res, next) {
	var statement = brookeDB.prepare('DELETE FROM notes WHERE id = ?');
	statement.run(req.params.id);
	statement.finalize();
	res.redirect('/admin/notes');
});

// Show posts
router.get('/posts', function(req, res, next) {
	blogDB.all('SELECT * FROM posts', function(err, rows) {
		rows = rows.map(function(row) {
			row.content = row.content.replace(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/g, '');
			return row;
		});
		res.render('admin/posts', { title: 'Blog Posts | Splotchbot Studios', posts: rows.reverse() });
	});
});

// Create new post page
router.get('/posts/new', function(req, res, next) {
	res.render('admin/newpost', { title: 'New blog post | Splotchbot Studios' });
});

// Submit new post
router.post('/posts/new', function(req, res, next) {
	var v = req.body;
	var statement = blogDB.prepare('INSERT INTO posts (title, content, posted_at) VALUES (?, ?, ?)');
	var dateObj = new Date();
	var dateString = '';
	dateString = dateString + (dateObj.getMonth() + 1) + '/' + dateObj.getDate() + '/' + dateObj.getFullYear();
	statement.run(v.title, v.content, dateString);
	statement.finalize();
	res.redirect('/admin/posts');
});

// Pull up editor page
router.get('/posts/:id', function(req, res, next) {
	blogDB.get('SELECT * FROM posts WHERE id = ?', req.params.id, function(err, row) {
		res.render('admin/postedit', { title: 'Edit post | Splotchbot Studios', post: row });
	});
});

// Submit editor thingy
router.post('/posts/:id', function(req, res, next) {
	var v = req.body;
	var statement = blogDB.prepare('UPDATE posts SET title = ?, content = ? WHERE id = ?');
	v.content = req.body.content.replace(new RegExp('(\r?\n){2}', 'g'), "<br><br>");
	statement.run(v.title, v.content, req.params.id);
	statement.finalize();
	res.redirect('/admin/posts');
});

// Delete the post
router.get('/posts/delete/:id', function(req, res, next) {
	var statement = blogDB.prepare('DELETE FROM posts WHERE id = ?');
	statement.run(req.params.id);
	statement.finalize();
	res.redirect('/admin/posts');
});

// Show messages
router.get('/messages', function(req, res, next) {
	brookeDB.all('SELECT * FROM messages', function (err, rows) {
		res.render('admin/messages', {title: 'Messages | Splotchbot Studios', messages: rows.reverse()});
	});
});

router.get('/messages/:id', function(req, res, next) {
	brookeDB.get('SELECT * FROM messages WHERE id = ?', req.params.id, function(err, row) {
		res.render('admin/showmessage', {title: row.subject + ' | Splotchbot Studios', message: row});
	});
});

router.get('/messages/delete/:id', function(req, res, next) {
	var statement = brookeDB.prepare('DELETE FROM messages WHERE id = ?');
	statement.run(req.params.id);
	statement.finalize();
	res.redirect('/admin/messages');
});

module.exports = router;
