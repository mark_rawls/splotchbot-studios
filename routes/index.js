var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3');
var db = new sqlite3.Database('db/posts.db');
var brookeDB = new sqlite3.Database('db/brooke.db');
var bcrypt = require('bcrypt');

/* GET home page. */
router.get('/', function(req, res, next) {
	db.get('SELECT * FROM posts ORDER BY id DESC LIMIT 1', function(err, row) {
		row.content = row.content.replace(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/g, '');
		res.render('index', { title: 'Home | Splotchbot Studios', post: row});
	});
});

router.get('/about', function(req, res, next) {
	res.render('about', { title: "About Us | Splotchbot Studios"});
});

router.get('/contact', function(req, res, next) {
	res.render('contact', {title: "Contact Us | Splotchbot Studios", post: false});
});

router.post('/contact', function(req, res, next) {
	var m = req.body;
	var statement = brookeDB.prepare('INSERT INTO MESSAGES (name, email, subject, message) VALUES (?, ?, ?, ?)');
	statement.run(m.name, m.email, m.subject, m.message);
	res.render('contact', {title: "Contact Us | Splotchbot Studios", post: true});
});

router.get('/team', function(req, res, next) {
	res.render('team', {title: "The Team | Splotchbot Studios"});
});

// Show sign-in page
router.get('/signin', function(req, res, next) {
	if (req.session.failedattempts >= 3) {
		res.render('admin/toomany', { title: 'Too many attempts! | Splotchbot Studios' });
	} else {
		res.render('admin/nocredentials', { title: 'Not logged in | Splotchbot Studios' });
	}
});

// Make the sign-in page work
router.post('/signin', function(req, res, next) {
	// Doublecheck that failed attempts exists
	if (typeof(req.session.failedattempts) === 'undefined') {
		req.session.failedattempts = 0;
	}


	if (req.session.failedattempts >= 3) {
		res.render('admin/toomany', { title: 'Too many attempts! | Splotchbot Studios' });
	}

	//Actually evaluate the goddamn password
	if (req.session.failedattempts <= 3 && bcrypt.compareSync(req.body.password, process.env.PASSWORD_HASH)) {
		req.session.admin = true;
		res.redirect('/admin');
	} else {
		res.render('admin/failedlogin', { title: 'Failed login! | Splotchbot Studios' });
	}
});


module.exports = router;
