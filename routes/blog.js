var express = require('express');
var sqlite3 = require('sqlite3');
var router = express.Router();
var db = new sqlite3.Database('db/posts.db');
var bcrypt = require('bcrypt');

/* GET home page. */
router.get('/', function(req, res, next) {
	if (req.params.page !== undefined) {
		if (req.params.page > 1) {
			db.all('SELECT * FROM posts', function(err, rows) {
				if (rows !== undefined) {
					rows.reverse();
					console.log(rows);
					rows.map(function(row) {
						row.content = row.content.replace(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/g, '');
					});
					res.render('blog/index', { title: 'Blog | Splotchbot Studios', posts: rows.slice((req.params.page - 1 * 25), req.params.page * 25), page: req.params.page });
				}
			});
		}
	} else {
		db.all('SELECT * FROM posts WHERE id < 26', function(err, rows) {
			rows.reverse();
			console.log(rows);
			rows.map(function(row) {
				row.content = row.content.replace(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/g, '');
			});
			res.render('blog/index', { title: 'Blog | Splotchbot Studios', posts: rows.slice(0, 25), page: req.params.page });
		});
	}
});

router.get('/post/:id', function(req, res, next) {
	db.get('SELECT * FROM posts WHERE ID = ?', req.params.id, function(err, row) {
		res.render('blog/view', { title: row.title + " | Splotchbot Studios", post: row });
	});
});


module.exports = router;
