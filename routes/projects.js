var express = require('express');
var router = express.Router();

router.get('/danny', function(req, res, next) {
	res.render('projects/danny', { title: 'Danny the Reaper | Splotchbot Studios' });
});

module.exports = router;